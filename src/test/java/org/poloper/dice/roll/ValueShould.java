package org.poloper.dice.roll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.poloper.dice.roll.Value.sequence;
import static org.poloper.dice.roll.Value.value;

public class ValueShould {
    @Test
    public void be_readable() {
        assertEquals("<true>", v(value(true)));
        assertEquals("<false>", v(value(false)));

        assertEquals("<5>", v(value(5)));
        assertEquals("{<3>, <5>}", v(sequence(value(3), value(5))));

        assertEquals("<5>|++", v(value(5).withTag("++")));
        assertEquals("<5>|A, B", v(value(5).withTag("A").withTag("B")));
        assertEquals("{<3>, <5>}|++", v(sequence(value(3), value(5)).withTag("++")));
    }

    @Test
    public void have_asNumber() {
        assertEquals(6, value(6).asNumber());
        assertEquals(3, sequence(value(1), value(2)).asNumber());
    }

    @Test
    public void have_asBoolean() {
        assertFalse(value(0).asBoolean());
        assertTrue(value(1).asBoolean());
        assertTrue(value(6).asBoolean());
        assertFalse(sequence().asBoolean());
        assertTrue(sequence(value(1), value(2)).asBoolean());
    }

    @Test
    public void have_asCollection() {
        assertEquals("{<true>}", v(sequence(value(true).asCollection())));
        assertEquals("{<5>}", v(sequence(value(5).asCollection())));
        assertEquals("{<1>, <2>}", v(sequence(sequence(value(1), value(2)).asCollection())));
    }

    private String v(final Value value) {
        return value.toString();
    }
}

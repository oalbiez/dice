package org.poloper.dice.roll;

import io.vavr.collection.List;
import org.junit.jupiter.api.Test;
import org.poloper.dice.expression.Expression;
import org.poloper.dice.random.RandomSource;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.poloper.dice.expression.Expressions.*;

public class InterpreterShould {

    @Test
    public void evaluate_dice() {
        assertTrue(Interpreter.create().eval(dice(6)).asNumber() > 0);
        assertTrue(Interpreter.create().eval(dice(6)).asNumber() <= 6);
    }

    @Test
    public void evaluate_dice_fate() {
        assertEquals("<-1>", v(DF(), rnd(1)));
        assertEquals("<-1>", v(DF(), rnd(2)));
        assertEquals("<0>", v(DF(), rnd(3)));
        assertEquals("<0>", v(DF(), rnd(4)));
        assertEquals("<1>", v(DF(), rnd(5)));
        assertEquals("<1>", v(DF(), rnd(6)));
    }

    @Test
    public void evaluate_constant() {
        assertEquals("<6>", v(constant(6)));
    }

    @Test
    public void evaluate_repeat() {
        assertEquals("{}", v(repeat(0, constant(1))));
        assertEquals("{<1>}", v(repeat(1, constant(1))));
        assertEquals("{<1>, <1>, <1>}", v(repeat(3, constant(1))));
    }

    @Test
    public void evaluate_sum() {
        assertEquals("<0>", v(sum(constant(0))));
        assertEquals("<3>", v(sum(repeat(constant(3), constant(1)))));
        assertEquals("<5>", v(sum(repeat(constant(2), dice(6))), rnd(2, 3)));
    }

    @Test
    public void evaluate_sub() {
        // TODO: the current implementation is incorrect
        assertEquals("<0>", v(sub(constant(0))));
        assertEquals("<-7>", v(sub(constant(4), constant(3))));
    }

    @Test
    public void evaluate_neg() {
        assertEquals("<-3>", v(neg(constant(3))));
        assertEquals("<3>", v(neg(constant(-3))));
    }

    @Test
    public void evaluate_explode() {
        assertEquals("<3>", v(explode(dice(6), lt(6)), rnd(3)));
        assertEquals("<5>", v(explode(dice(6), lt(6)), rnd(5)));
        assertEquals("<11>", v(explode(dice(6), lt(6)), rnd(6, 5)));
        assertEquals("<17>", v(explode(dice(6), lt(6)), rnd(6, 6, 5)));
    }

    @Test
    public void evaluate_sequence() {
        assertEquals("{<3>, <4>}", v(sequence(constant(3), constant(4))));
    }

    @Test
    public void evaluate_ascending_and_descending() {
        assertEquals("{<4>, <6>}", v(ascending(constant(6), constant(4))));
        assertEquals("{<6>, <4>}", v(descending(constant(6), constant(4))));
    }

    @Test
    public void evaluate_first() {
        assertEquals("{<1>, <2>}", v(first(2, sequence(constant(1), constant(2)))));
        assertEquals("{<1>, <2>}", v(first(2, sequence(constant(1), constant(2), constant(3)))));
        assertEquals("{<1>}", v(first(1, sequence(constant(1), constant(2)))));
        assertEquals("{}", v(first(0, sequence(constant(1), constant(2)))));
        assertEquals("{<1>}", v(first(sequence(constant(1), constant(2)))));
    }

    @Test
    public void evaluate_max() {
        assertEquals("<2>", v(max(constant(1), constant(2))));
        assertEquals("<6>", v(max(dice(6), dice(20)), rnd(6, 4)));
    }

    @Test
    public void evaluate_min() {
        assertEquals("<1>", v(min(constant(1), constant(2))));
        assertEquals("<6>", v(min(dice(6), dice(20)), rnd(6, 14)));
    }


    @Test
    public void evaluate_last() {
        assertEquals("{<1>, <2>}", v(last(2, sequence(constant(1), constant(2)))));
        assertEquals("{<2>, <3>}", v(last(2, sequence(constant(1), constant(2), constant(3)))));
        assertEquals("{<2>}", v(last(1, sequence(constant(1), constant(2)))));
        assertEquals("{}", v(last(0, sequence(constant(1), constant(2)))));
        assertEquals("{<2>}", v(last(sequence(constant(1), constant(2)))));
    }

    @Test
    public void evaluate_rollUntil() {
        assertEquals("{<3>}", v(rollUntil(dice(6), lt(6)), rnd(3)));
        assertEquals("{<5>}", v(rollUntil(dice(6), lt(6)), rnd(5)));
        assertEquals("{<6>, <5>}", v(rollUntil(dice(6), lt(6)), rnd(6, 5)));
        assertEquals("{<6>, <6>, <5>}", v(rollUntil(dice(6), lt(6)), rnd(6, 6, 5)));
    }

    @Test
    public void evaluate_reRoll() {
        assertEquals("<3>", v(reRoll(dice(6), gt(2), le(2)), rnd(3)));
        assertEquals("<3>", v(reRoll(dice(6), gt(2), le(2)), rnd(1, 3)));
        assertEquals("<1>", v(reRoll(dice(6), gt(2), le(2)), rnd(2, 1)));
    }

    @Test
    public void evaluate_keepHighest() {
        assertEquals("{<6>, <4>}", v(keepHighest(dice(6), constant(3), constant(2)), rnd(3, 6, 4)));
    }

    @Test
    public void evaluate_keepLowest() {
        assertEquals("{<3>, <4>}", v(keepLowest(dice(6), constant(3), constant(2)), rnd(3, 6, 4)));
    }

    @Test
    public void evaluate_abs() {
        assertEquals("<1>", v(abs(constant(1))));
        assertEquals("<1>", v(abs(constant(-1))));
    }

    @Test
    public void evaluate_mul() {
        assertEquals("<8>", v(mul(constant(2), constant(4))));
    }

    @Test
    public void evaluate_and() {
        assertEquals("<false>", v(and(constant(false))));
        assertEquals("<false>", v(and(constant(false), constant(false))));
        assertEquals("<false>", v(and(constant(false), constant(true))));
        assertEquals("<false>", v(and(constant(true), constant(false))));
        assertEquals("<true>", v(and(constant(true), constant(true))));
    }

    @Test
    public void evaluate_or() {
        assertEquals("<false>", v(or(constant(false))));
        assertEquals("<false>", v(or(constant(false), constant(false))));
        assertEquals("<true>", v(or(constant(false), constant(true))));
        assertEquals("<true>", v(or(constant(true), constant(false))));
        assertEquals("<true>", v(or(constant(true), constant(true))));
    }

    @Test
    public void evaluate_not() {
        assertEquals("<true>", v(not(constant(false))));
        assertEquals("<false>", v(not(constant(true))));
    }

    @Test
    public void evaluate_with() {
        assertEquals("<3>", v(with(var("a"), def("a", constant(3)))));
    }

    @Test
    public void evaluate_name() {
        assertEquals("{<6>}", v(last(name(constant(6), "a"), var("a"))));
    }

    @Test
    public void evaluate_tag() {
        final Expression expression = tags(
                name(dice(6), "A"),
                when(eq(var("A"), constant(6)), "!"));
        assertEquals("<3>", v(expression, rnd(3)));
        assertEquals("<6>|!", v(expression, rnd(6)));
    }

    @Test
    public void evaluate_has_tag() {
        final Expression expression = tags(
                name(dice(6), "A"),
                when(eq(var("A"), constant(6)), "!"));
        assertEquals("<false>", v(hasTag(expression, "!"), rnd(3)));
        assertEquals("<true>", v(hasTag(expression, "!"), rnd(6)));
    }

    private String v(final Expression expression) {
        return Interpreter.create().eval(expression).toString();
    }

    private String v(final Expression expression, final RandomSource source) {
        return Interpreter.create(source).eval(expression).toString();
    }

    private RandomSource rnd(final long... values) {
        final Iterator<Long> it = List.ofAll(values).iterator();
        return upper -> it.next();
    }
}

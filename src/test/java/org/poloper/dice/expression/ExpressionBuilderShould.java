package org.poloper.dice.expression;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.poloper.dice.expression.Expressions.*;

public class ExpressionBuilderShould {
    @Test
    public void reject_repeat_with_n_not_constant() {
        assertThrows(RuntimeException.class, () -> repeat(dice(6), dice(6)));
    }

    @Test
    public void reject_rollUntil_with_roll_constant() {
        assertThrows(RuntimeException.class, () -> rollUntil(constant(3), eq(6), constant(1)));
    }

    @Test
    public void reject_first_with_n_not_constant() {
        assertThrows(RuntimeException.class, () -> first(dice(6), dice(6)));
    }

    @Test
    public void reject_last_with_n_not_constant() {
        assertThrows(RuntimeException.class, () -> last(dice(6), dice(6)));
    }
}

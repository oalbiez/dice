package org.poloper.dice.expression;

import org.junit.jupiter.api.Test;
import org.poloper.dice.roll.Interpreter;
import org.poloper.dice.roll.Value;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.poloper.dice.expression.Expressions.*;
import static org.poloper.dice.roll.Value.value;

public class ValuePredicateTest {
    @Test
    public void equality() {
        assertTrue(check(value(10), eq(10)));
        assertTrue(check(value(10), eq(10)));

        assertFalse(check(value(9), eq(10)));
        assertFalse(check(value(9), eq(10)));
    }

    @Test
    public void not_equal() {
        assertFalse(check(value(10), neq(10)));
        assertFalse(check(value(10), neq(10)));

        assertTrue(check(value(9), neq(10)));
        assertTrue(check(value(9), neq(10)));
    }

    @Test
    public void greater_or_equal() {
        assertTrue(check(value(10), ge(10)));
        assertTrue(check(value(20), ge(10)));
        assertTrue(check(value(10), ge(10)));
        assertTrue(check(value(20), ge(10)));

        assertFalse(check(value(9), ge(10)));
        assertFalse(check(value(9), ge(10)));
    }

    @Test
    public void greater_than() {
        assertTrue(check(value(11), gt(10)));
        assertTrue(check(value(20), gt(10)));
        assertTrue(check(value(11), gt(10)));
        assertTrue(check(value(20), gt(10)));

        assertFalse(check(value(9), gt(10)));
        assertFalse(check(value(10), gt(10)));
        assertFalse(check(value(9), gt(10)));
        assertFalse(check(value(10), gt(10)));
    }

    @Test
    public void less_or_equal() {
        assertTrue(check(value(10), le(10)));
        assertTrue(check(value(9), le(10)));
        assertTrue(check(value(10), le(10)));
        assertTrue(check(value(9), le(10)));

        assertFalse(check(value(11), le(10)));
        assertFalse(check(value(11), le(10)));
    }

    @Test
    public void less_than() {
        assertTrue(check(value(9), lt(10)));
        assertTrue(check(value(1), lt(10)));
        assertTrue(check(value(9), lt(10)));
        assertTrue(check(value(1), lt(10)));

        assertFalse(check(value(10), lt(10)));
        assertFalse(check(value(11), lt(10)));
        assertFalse(check(value(10), lt(10)));
        assertFalse(check(value(11), lt(10)));
    }

    private boolean check(final Value value, final Expression predicate) {
        return Interpreter.create().withContext(value).eval(predicate).asBoolean();
    }
}

package org.poloper.dice.expression;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.poloper.dice.expression.Expressions.*;

public class ConstantExpressionTest {

    @Test
    public void dice_is_never_constant() {
        assertFalse(dice(6).constant());
    }

    @Test
    public void constant_is_always_constant() {
        assertTrue(constant(6).constant());
    }

    @Test
    public void constant_on_abs() {
        assertFalse(abs(dice(6)).constant());
        assertTrue(abs(constant(6)).constant());
    }

    @Test
    public void constant_on_first() {
        assertFalse(first(constant(6), dice(6)).constant());
        assertTrue(first(constant(6), constant(6)).constant());
    }

    @Test
    public void constant_on_last() {
        assertFalse(last(constant(6), dice(6)).constant());
        assertTrue(last(constant(6), constant(6)).constant());
    }

    @Test
    public void constant_on_max() {
        assertFalse(max(dice(6)).constant());
        assertTrue(max(constant(6)).constant());
    }

    @Test
    public void constant_on_min() {
        assertFalse(min(dice(6)).constant());
        assertTrue(min(constant(6)).constant());
    }

    @Test
    public void constant_on_mul() {
        assertFalse(mul(dice(6), dice(6)).constant());
        assertFalse(mul(constant(6), dice(6)).constant());
        assertFalse(mul(dice(6), constant(6)).constant());
        assertTrue(mul(constant(6), constant(6)).constant());
    }

    @Test
    public void constant_on_repeat() {
        assertFalse(repeat(constant(6), dice(6)).constant());
        assertTrue(repeat(constant(6), constant(6)).constant());
    }

    @Test
    public void constant_on_rollUntil() {
        assertFalse(rollUntil(dice(6), eq(6), constant(1)).constant());
    }

    @Test
    public void constant_on_sequence() {
        assertFalse(sequence(dice(6), dice(6)).constant());
        assertFalse(sequence(constant(6), dice(6)).constant());
        assertFalse(sequence(dice(6), constant(6)).constant());
        assertTrue(sequence(constant(6), constant(6)).constant());
    }

    @Test
    public void constant_on_sort() {
        assertFalse(descending(dice(6)).constant());
        assertTrue(descending(constant(6)).constant());
    }

    @Test
    public void constant_on_sum() {
        assertFalse(sum(dice(6)).constant());
        assertTrue(sum(constant(6)).constant());
    }
}

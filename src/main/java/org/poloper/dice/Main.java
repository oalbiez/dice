package org.poloper.dice;


import org.poloper.dice.expression.Expression;
import org.poloper.dice.expression.Expressions;
import org.poloper.dice.statistics.StatisticsResult;
import org.poloper.dice.statistics.VerticalBarGraph;

import static org.poloper.dice.expression.Expressions.*;
import static org.poloper.dice.statistics.Statistics.monteCarlo;

public final class Main {
    public static void main(final String[] args) {
        final Expression expression = co();

        final StatisticsResult result = monteCarlo(expression, 10000);
        System.out.println(VerticalBarGraph.fromLong(result.samples(), 120).render());
        System.out.println("normal result probability : " + result.probability(roll -> roll.tags().isEmpty()));
        System.out.println("Critical success probability : " + result.probability(roll -> roll.tags().contains("++")));
        System.out.println("Critical failure probability : " + result.probability(roll -> roll.tags().contains("--")));
        System.out.println("Total : " + result.total());
    }

    private static Expression fate() {
        return sum(repeat(4, DF()));
    }

    private static Expression co() {
        return with(sum(explode(dice(6), or(lt(6), hasTag(var("white"), "--"))), var("white")),
                def("white", tags(
                        sum(explode(name(dice(6), "A"), lt(6)), explode(name(dice(6), "B"), lt(6))),
                        when(and(Expressions.eq(var("A"), constant(1)), Expressions.eq(var("B"), constant(1))), "--"),
                        when(and(Expressions.eq(var("A"), constant(6)), Expressions.eq(var("B"), constant(6))), "++"))));
    }
}

package org.poloper.dice.roll;

import io.vavr.collection.List;

import static java.util.stream.Collectors.joining;

public abstract class Value {
    public static Value min() {
        return value(Long.MIN_VALUE);
    }

    public static Value max() {
        return value(Long.MAX_VALUE);
    }

    public static Value value(final boolean value) {
        return new Boolean(value, List.empty());
    }

    public static Value value(final long value) {
        return new Number(value, List.empty());
    }

    public static Value sequence(final Value... values) {
        return sequence(List.of(values));
    }

    public static Value sequence(final List<Value> values) {
        return new Sequence(values, List.empty());
    }

    protected final List<String> _tags;

    protected Value(final List<String> tags) {
        _tags = tags;
    }

    public abstract boolean asBoolean();

    public abstract long asNumber();

    public abstract List<Value> asCollection();

    public List<String> tags() {
        return _tags;
    }

    public abstract Value withTag(final String tag);

    public abstract Value withTags(final List<String> tags);

    public static final class Boolean extends Value {
        private final boolean _value;

        private Boolean(final boolean value, final List<String> tags) {
            super(tags);
            _value = value;
        }

        public boolean asBoolean() {
            return _value;
        }

        public long asNumber() {
            return _value ? 1 : 0;
        }

        public List<Value> asCollection() {
            return List.of(this);
        }

        public Value withTag(final String tag) {
            return new Boolean(_value, _tags.append(tag));
        }

        public Value withTags(final List<String> tags) {
            return new Boolean(_value, _tags.appendAll(tags));
        }

        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder();
            builder.append("<").append(_value).append(">");
            if (!_tags.isEmpty()) {
                builder.append("|");
                builder.append(_tags.collect(joining(", ")));
            }
            return builder.toString();
        }
    }

    public static final class Number extends Value {
        private final long _value;

        private Number(final long value, final List<String> tags) {
            super(tags);
            _value = value;
        }

        public long asNumber() {
            return _value;
        }

        public boolean asBoolean() {
            return _value != 0;
        }

        public List<Value> asCollection() {
            return List.of(this);
        }

        public Value withTag(final String tag) {
            return new Number(_value, _tags.append(tag));
        }

        public Value withTags(final List<String> tags) {
            return new Number(_value, _tags.appendAll(tags));
        }

        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder();
            builder.append("<").append(_value).append(">");
            if (!_tags.isEmpty()) {
                builder.append("|");
                builder.append(_tags.collect(joining(", ")));
            }
            return builder.toString();
        }
    }

    public static class Sequence extends Value {
        private final List<Value> _values;

        Sequence(final List<Value> values, final List<String> tags) {
            super(tags);
            _values = values;
        }

        public long asNumber() {
            return _values.map(Value::asNumber).sum().longValue();
        }

        public boolean asBoolean() {
            return !_values.isEmpty();
        }

        public List<Value> asCollection() {
            return _values;
        }

        public Value withTag(final String tag) {
            return new Sequence(_values, _tags.append(tag));
        }

        public Value withTags(final List<String> tags) {
            return new Sequence(_values, _tags.appendAll(tags));
        }

        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder();
            builder.append(_values
                    .map(Object::toString)
                    .collect(joining(", ", "{", "}")));
            if (!_tags.isEmpty()) {
                builder.append("|");
                builder.append(_tags.collect(joining(", ")));
            }
            return builder.toString();
        }
    }
}

package org.poloper.dice.roll;

import io.vavr.collection.List;

import java.util.Comparator;
import java.util.Objects;

import static java.util.stream.Collectors.joining;

public final class Roll implements Comparable<Roll> {
    private final long _value;
    private final List<String> _tags;

    public static Roll of(final long value, final List<String> tags) {
        return new Roll(value, tags);
    }

    private Roll(final long value, final List<String> tags) {
        _value = value;
        _tags = tags;
    }

    public long value() {
        return _value;
    }

    public List<String> tags() {
        return _tags;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(_value);
        if (!_tags.isEmpty()) {
            builder.append("|");
            builder.append(_tags.collect(joining(", ")));
        }
        return builder.toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Roll roll = (Roll) o;
        return _value == roll._value && _tags.equals(roll._tags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(_value, _tags);
    }

    @Override
    public int compareTo(final Roll other) {
        return Comparator
                .comparingLong(Roll::value)
                .thenComparing(r -> r.tags().toString())
                .compare(this, other);
    }
}

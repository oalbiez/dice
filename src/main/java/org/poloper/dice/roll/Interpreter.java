package org.poloper.dice.roll;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import org.poloper.dice.expression.*;
import org.poloper.dice.random.RandomSource;
import org.poloper.dice.random.SecureRandomSource;

import java.util.Comparator;
import java.util.function.BiFunction;

import static org.poloper.dice.roll.Value.sequence;
import static org.poloper.dice.roll.Value.value;


public class Interpreter implements ExpressionVisitor<Value> {
    private final RandomSource randomSource;
    private Map<String, Value> symbols;

    private Interpreter(final RandomSource randomSource, final Map<String, Value> symbols) {
        this.randomSource = randomSource;
        this.symbols = symbols;
    }

    public static Interpreter create() {
        return create(new SecureRandomSource());
    }

    public static Interpreter create(final RandomSource source) {
        return new Interpreter(source, HashMap.empty());
    }

    public Interpreter withSymbol(final String name, final Value value) {
        return new Interpreter(randomSource, symbols.put(name, value));
    }

    public Interpreter withContext(final Value value) {
        return withSymbol("_", value);
    }

    public Value eval(final Expression expression) {
        return expression.accept(this);
    }

    public Roll roll(final Expression expression) {
        final Value result = eval(expression);
        return Roll.of(result.asNumber(), result.tags());
    }

    public Value visitBinaryOperator(final BinaryOperator expression) {
        switch (expression.operator()) {
            case AND -> {
                return reduceBoolean(expression.sequence(), value(true), (x, y) -> x && y);
            }
            case OR -> {
                return reduceBoolean(expression.sequence(), value(false), (x, y) -> x || y);
            }
            case ADD -> {
                return reduceNumber(expression.sequence(), value(0), Long::sum);
            }
            case MUL -> {
                return reduceNumber(expression.sequence(), value(1), (x, y) -> x * y);
            }
            case SUB -> {
                return reduceNumber(expression.sequence(), value(0), (x, y) -> x - y);
            }
            case MIN -> {
                return reduceNumber(expression.sequence(), Value.max(), Math::min);
            }
            case MAX -> {
                return reduceNumber(expression.sequence(), Value.min(), Math::max);
            }
        }
        throw new RuntimeException("invalid operator: " + expression.operator());
    }

    public Value visitBooleanLiteral(final BooleanLiteral expression) {
        return value(expression.value());
    }

    public Value visitComparison(final Comparison expression) {
        final Value left = eval(expression.left());
        final Value right = eval(expression.right());
        switch (expression.operator()) {
            case EQ -> {
                return withTags(value(left.asNumber() == right.asNumber()), left, right);
            }
            case NEQ -> {
                return withTags(value(left.asNumber() != right.asNumber()), left, right);
            }
            case GT -> {
                return withTags(value(left.asNumber() > right.asNumber()), left, right);
            }
            case GE -> {
                return withTags(value(left.asNumber() >= right.asNumber()), left, right);
            }
            case LT -> {
                return withTags(value(left.asNumber() < right.asNumber()), left, right);
            }
            case LE -> {
                return withTags(value(left.asNumber() <= right.asNumber()), left, right);
            }
        }
        throw new RuntimeException("invalid operator: " + expression.operator());
    }

    public Value visitDice(final Dice expression) {
        return value(randomSource.random(expression.faces()));
    }

    public Value visitDiceFate(final DiceFate expression) {
        final int random = (int) randomSource.random(6);
        return switch (random) {
            case 1, 2 -> value(-1);
            case 3, 4 -> value(0);
            default -> value(1);
        };
    }

    public Value visitHasTags(final HasTags expression) {
        return value(eval(expression.expression()).tags().containsAll(expression.tags()));
    }

    public Value visitLet(final Let expression) {
        Interpreter interpreter = this;
        for (final Definition definition : expression.definitions()) {
            interpreter = interpreter.withSymbol(definition.name(), eval(definition.expression()));
        }
        return interpreter.eval(expression.expression());
    }

    public Value visitName(final Name expression) {
        final Value result = eval(expression.child());
        if (!symbols.containsKey(expression.name())) {
            symbols = symbols.put(expression.name(), result);
        }
        return result;
    }

    public Value visitNumberLiteral(final NumberLiteral expression) {
        return value(expression.value());
    }

    public Value visitRepeat(final Repeat expression) {
        return sequence(List.fill((int) eval(expression.n()).asNumber(), () -> eval(expression.roll())));
    }

    public Value visitRollUntil(final RollUntil expression) {
        List<Value> rolls = List.empty();
        int count = 1;
        while (evalPredicate(expression.limit(), Value.value(count))) {
            final Value current = eval(expression.roll());
            count++;
            rolls = rolls.append(current);
            if (evalPredicate(expression.predicate(), current)) break;
        }
        return sequence(rolls);
    }

    public Value visitSequence(final Sequence expression) {
        return sequence(expression.items().map(this::eval));
    }

    public Value visitSort(final Sort sort) {
        Comparator<Value> comparator = Comparator.comparingLong(Value::asNumber);
        if (sort.direction() == Sort.Direction.DESCENDING) {
            comparator = comparator.reversed();
        }
        return sequence(eval(sort.sequence()).asCollection().sorted(comparator));
    }

    public Value visitTags(final Tags expression) {
        Value result = eval(expression.expression());
        for (final Case aCase : expression.cases()) {
            if (eval(aCase.expression()).asBoolean()) {
                result = result.withTag(aCase.tag());
            }
        }
        return result;
    }

    public Value visitTake(final Take expression) {
        final int count = (int) eval(expression.n()).asNumber();
        final List<Value> values = eval(expression.sequence()).asCollection();
        switch (expression.operator()) {
            case FIRST -> {
                return sequence(values.take(count));
            }
            case LAST -> {
                return sequence(values.takeRight(count));
            }
        }
        throw new RuntimeException("invalid operator: " + expression.operator());
    }

    public Value visitUnary(final UnaryOperator expression) {
        final Value value = eval(expression.child());
        switch (expression.operator()) {
            case NOT -> {
                return withTags(value(!value.asBoolean()), value);
            }
            case NEG -> {
                return withTags(value(-value.asNumber()), value);
            }
            case ABS -> {
                return withTags(value(Math.abs(value.asNumber())), value);
            }
        }
        throw new RuntimeException("invalid operator: " + expression.operator());
    }

    public Value visitVar(final Var expression) {
        return symbols.get(expression.name()).getOrElseThrow(() -> new RuntimeException("symbol not found" + expression.name()));
    }

    private boolean evalPredicate(final Expression expression, final Value value) {
        return expression.accept(this.withContext(value)).asBoolean();
    }

    private Value reduceNumber(final Expression sequence, final Value unit, final BiFunction<Long, Long, Long> op) {
        final Value seq = eval(sequence);
        final List<Value> results = seq.asCollection();
        if (results.isEmpty()) {
            return unit.withTags(seq.tags());
        }
        return withTags(
                value(results.map(Value::asNumber).fold(unit.asNumber(), op)),
                results);
    }

    private Value reduceBoolean(final Expression sequence, final Value unit, final BiFunction<Boolean, Boolean, Boolean> op) {
        final Value seq = eval(sequence);
        final List<Value> results = seq.asCollection();
        if (results.isEmpty()) {
            return unit.withTags(seq.tags());
        }
        return withTags(
                value(results.map(Value::asBoolean).fold(unit.asBoolean(), op)),
                results);
    }

    private Value withTags(final Value result, final List<Value> values) {
        return result.withTags(values.map(Value::tags).fold(List.empty(), List::appendAll));
    }

    private Value withTags(final Value result, final Value... values) {
        return withTags(result, List.of(values));
    }
}

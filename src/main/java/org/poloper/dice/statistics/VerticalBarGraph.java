package org.poloper.dice.statistics;

import io.vavr.collection.Map;
import org.poloper.dice.roll.Roll;

import static com.google.common.base.Strings.padEnd;
import static com.google.common.base.Strings.padStart;

public final class VerticalBarGraph {
    private static final char[] BLOCKS = new char[]{' ', '▏', '▎', '▍', '▌', '▋', '▊', '█'};

    private final Map<Roll, Percent> samples;
    private final double ratio;
    private final int labelWidth;

    public static VerticalBarGraph fromLong(final Samples samples, final long width) {
        return new VerticalBarGraph(samples.normalize(), width);
    }

    private VerticalBarGraph(final Map<Roll, Percent> samples, final long width) {
        this.samples = samples;
        ratio = 7 * width / samples.values().max().getOrElse(Percent.of(1.0)).value();
        labelWidth = samples.keySet().map(Roll::toString).map(String::length).max().getOrElse(0);
    }

    public String render() {
        final StringBuilder builder = new StringBuilder();
        samples.forEach(tuple -> builder.append(render(tuple._1, tuple._2)).append("\n"));
        return builder.toString();
    }

    private String render(final Roll value, final Percent percent) {
        return padEnd(value.toString(), labelWidth, ' ') +
                " │ " +
                padStart(percent.toString(), 7, ' ') +
                " " +
                barFor(percent);
    }

    private String barFor(final Percent percent) {
        final int size = Math.toIntExact(Math.round(ratio * percent.value()));
        final int nbFull = size / 7;
        final int partial = size % 7;
        return "█".repeat(nbFull) + BLOCKS[partial];
    }
}

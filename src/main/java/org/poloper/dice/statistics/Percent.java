package org.poloper.dice.statistics;

import java.util.Objects;

import static java.lang.String.format;

public final class Percent implements Comparable<Percent> {
    private final double _value;

    public static Percent of(final double value) {
        return new Percent(value);
    }

    private Percent(final double value) {
        _value = value;
    }

    public double value() {
        return _value;
    }

    @Override
    public String toString() {
        return format("%.2f", _value * 100.0) + "%";
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Percent percent = (Percent) o;
        return Double.compare(percent._value, _value) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(_value);
    }

    @Override
    public int compareTo(final Percent other) {
        return Double.compare(value(), other.value());
    }
}

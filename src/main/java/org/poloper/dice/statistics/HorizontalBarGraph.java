package org.poloper.dice.statistics;

import io.vavr.collection.Map;

public class HorizontalBarGraph {
    private static final char[] blocks = new char[]{'▁', '▂', '▃', '▄', '▅', '▆', '▇', '█'};
    private final Map<Long, Double> samples;
    private final int height;

    public HorizontalBarGraph(final Map<Long, Double> samples, final int height) {
        this.samples = samples;
        this.height = height;
    }
}

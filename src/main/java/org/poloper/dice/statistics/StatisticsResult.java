package org.poloper.dice.statistics;

import org.poloper.dice.expression.Expression;
import org.poloper.dice.roll.Roll;

import java.util.function.Predicate;

public final class StatisticsResult {
    private final Expression expression;
    private final Samples samples;
    private final double delta;

    public static StatisticsResult on(final Expression expression) {
        return new StatisticsResult(expression, Samples.empty(), 1.0);
    }

    private StatisticsResult(final Expression expression, final Samples samples, final double delta) {
        this.expression = expression;
        this.samples = samples;
        this.delta = delta;
    }

    public double delta() {
        return delta;
    }

    public long total() {
        return samples.total();
    }

    public Samples samples() {
        return samples;
    }

    public StatisticsResult add(final Roll value) {
        final Samples next = samples.add(value);
        return new StatisticsResult(expression, next, Samples.delta(samples, next));
    }

    public Percent probability(final Predicate<Roll> predicate) {
        return samples.probability(predicate);
    }
}

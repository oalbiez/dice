package org.poloper.dice.statistics;

import io.vavr.collection.SortedMap;
import io.vavr.collection.TreeMap;
import org.poloper.dice.roll.Roll;

import java.util.function.Predicate;

public final class Samples {
    private final SortedMap<Roll, Long> samples;
    private final long total;

    public static Samples empty() {
        return new Samples(TreeMap.empty(), 0);
    }

    public static double delta(final Samples left, final Samples right) {
        final SortedMap<Roll, Percent> l = left.normalize();
        final SortedMap<Roll, Percent> r = right.normalize();
        return l
                .merge(r, (x, y) -> Percent.of(sqr(x.value() - y.value())))
                .values()
                .map(Percent::value)
                .sum()
                .doubleValue();
    }

    private Samples(final SortedMap<Roll, Long> samples, final long total) {
        this.samples = samples;
        this.total = total;
    }

    public long total() {
        return total;
    }

    public Samples add(final Roll value) {
        return new Samples(samples.put(value, 1L, Long::sum), total + 1);
    }

    public SortedMap<Roll, Percent> normalize() {
        return samples.mapValues(v -> v.doubleValue() / total).mapValues(Percent::of);
    }

    public Percent probability(final Predicate<Roll> predicate) {
        return Percent.of(samples.filterKeys(predicate).values().sum().doubleValue() / total);
    }

    private static double sqr(final double v) {
        return v * v;
    }
}

package org.poloper.dice.statistics;

import org.poloper.dice.expression.Expression;
import org.poloper.dice.roll.Interpreter;

public final class Statistics {
    public static StatisticsResult monteCarlo(final Expression expression, final long iterations) {
        StatisticsResult result = StatisticsResult.on(expression);
        for (long i = 0; i < iterations; i++) {
            result = result.add(Interpreter.create().roll(expression));
        }
        return result;
    }

    public static StatisticsResult monteCarlo(final Expression expression, final double precision) {
        StatisticsResult result = StatisticsResult.on(expression);
        while (result.delta() > precision) {
            result = result.add(Interpreter.create().roll(expression));
        }
        return result;
    }

}

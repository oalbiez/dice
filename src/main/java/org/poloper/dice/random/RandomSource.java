package org.poloper.dice.random;

public interface RandomSource {
    long random(int upper);
}

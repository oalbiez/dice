package org.poloper.dice.random;

import java.security.SecureRandom;
import java.util.Random;

public class SecureRandomSource implements RandomSource {
    private final Random random = new SecureRandom();

    public long random(final int upper) {
        return random.nextInt(upper) + 1;
    }
}

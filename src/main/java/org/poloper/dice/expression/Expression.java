package org.poloper.dice.expression;

public interface Expression {
    <T> T accept(final ExpressionVisitor<T> visitor);

    boolean constant();
}

package org.poloper.dice.expression;

public final class UnaryOperator implements Expression {
    public enum Operator {NOT, NEG, ABS}

    private final Operator _operator;
    private final Expression _child;

    public UnaryOperator(final Operator operator, final Expression child) {
        _operator = operator;
        _child = child;
    }

    public Operator operator() {
        return _operator;
    }

    public Expression child() {
        return _child;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitUnary(this);
    }

    public boolean constant() {
        return _child.constant();
    }
}

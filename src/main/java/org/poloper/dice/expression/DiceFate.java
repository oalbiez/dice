package org.poloper.dice.expression;

public final class DiceFate implements Expression {

    DiceFate() {
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitDiceFate(this);
    }

    public boolean constant() {
        return false;
    }
}

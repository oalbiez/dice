package org.poloper.dice.expression;

public final class Comparison implements Expression {
    public enum Operator {EQ, NEQ, GT, GE, LT, LE}

    private final Operator _operator;
    private final Expression _left;
    private final Expression _right;

    public Comparison(final Operator operator, final Expression left, final Expression right) {
        _operator = operator;
        _left = left;
        _right = right;
    }

    public Operator operator() {
        return _operator;
    }

    public Expression left() {
        return _left;
    }

    public Expression right() {
        return _right;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitComparison(this);
    }

    public boolean constant() {
        return _left.constant() && _right.constant();
    }
}

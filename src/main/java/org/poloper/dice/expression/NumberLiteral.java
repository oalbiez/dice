package org.poloper.dice.expression;

public final class NumberLiteral implements Expression {
    private final int _value;

    NumberLiteral(final int value) {
        _value = value;
    }

    public int value() {
        return _value;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitNumberLiteral(this);
    }

    public boolean constant() {
        return true;
    }
}

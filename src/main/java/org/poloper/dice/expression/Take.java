package org.poloper.dice.expression;

public final class Take implements Expression {
    public enum Operator {FIRST, LAST}

    private final Operator _operator;
    private final Expression _n;
    private final Expression _sequence;

    public Take(final Operator operator, final Expression n, final Expression sequence) {
        _operator = operator;
        _n = n;
        _sequence = sequence;
    }

    public Operator operator() {
        return _operator;
    }

    public Expression n() {
        return _n;
    }

    public Expression sequence() {
        return _sequence;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitTake(this);
    }

    public boolean constant() {
        return _n.constant() && _sequence.constant();
    }
}

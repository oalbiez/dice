package org.poloper.dice.expression;

import io.vavr.collection.List;

public final class Let implements Expression {
    private final List<Definition> _definitions;
    private final Expression _expression;

    public Let(final List<Definition> definitions, final Expression expression) {
        _definitions = definitions;
        _expression = expression;
    }

    public List<Definition> definitions() {
        return _definitions;
    }

    public Expression expression() {
        return _expression;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitLet(this);
    }

    public boolean constant() {
        return false;
    }
}

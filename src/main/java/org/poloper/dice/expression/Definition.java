package org.poloper.dice.expression;

public final class Definition {
    private final String _name;
    private final Expression _expression;

    Definition(final String name, final Expression expression) {
        _name = name;
        _expression = expression;
    }

    public String name() {
        return _name;
    }

    public Expression expression() {
        return _expression;
    }
}

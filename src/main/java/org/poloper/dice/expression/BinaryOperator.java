package org.poloper.dice.expression;

public final class BinaryOperator implements Expression {
    public enum Operator {AND, OR, ADD, MUL, SUB, MIN, MAX}

    private final Operator _operator;
    private final Expression _sequence;

    public BinaryOperator(final Operator operator, final Expression sequence) {
        _operator = operator;
        _sequence = sequence;
    }

    public Operator operator() {
        return _operator;
    }

    public Expression sequence() {
        return _sequence;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitBinaryOperator(this);
    }

    public boolean constant() {
        return _sequence.constant();
    }
}

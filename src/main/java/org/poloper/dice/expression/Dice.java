package org.poloper.dice.expression;

public final class Dice implements Expression {
    private final int _faces;

    Dice(final int faces) {
        _faces = faces;
    }

    public int faces() {
        return _faces;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitDice(this);
    }

    public boolean constant() {
        return false;
    }
}

package org.poloper.dice.expression;

import io.vavr.collection.List;

public final class HasTags implements Expression {
    private final Expression _expression;
    private final List<String> _tags;

    public HasTags(final Expression expression, final List<String> tags) {
        _expression = expression;
        _tags = tags;
    }

    public Expression expression() {
        return _expression;
    }

    public List<String> tags() {
        return _tags;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitHasTags(this);
    }

    public boolean constant() {
        return _expression.constant();
    }
}

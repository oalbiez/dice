package org.poloper.dice.expression;

public final class RollUntil implements Expression {
    private final Expression _roll;
    private final Expression _predicate;
    private final Expression _limit;

    public RollUntil(final Expression roll, final Expression predicate, final Expression limit) {
        _roll = roll;
        _predicate = predicate;
        _limit = limit;
    }

    public Expression roll() {
        return _roll;
    }

    public Expression predicate() {
        return _predicate;
    }

    public Expression limit() {
        return _limit;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitRollUntil(this);
    }

    public boolean constant() {
        return _roll.constant();
    }
}

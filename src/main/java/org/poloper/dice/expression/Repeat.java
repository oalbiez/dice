package org.poloper.dice.expression;

public final class Repeat implements Expression {
    private final Expression _n;
    private final Expression _roll;

    public Repeat(final Expression n, final Expression roll) {
        _n = n;
        _roll = roll;
    }

    public Expression n() {
        return _n;
    }

    public Expression roll() {
        return _roll;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitRepeat(this);
    }

    public boolean constant() {
        return _n.constant() && _roll.constant();
    }
}

package org.poloper.dice.expression;

public final class BooleanLiteral implements Expression {
    private final boolean _value;

    BooleanLiteral(final boolean value) {
        _value = value;
    }

    public boolean value() {
        return _value;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitBooleanLiteral(this);
    }

    public boolean constant() {
        return true;
    }
}

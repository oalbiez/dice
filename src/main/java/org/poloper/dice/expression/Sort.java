package org.poloper.dice.expression;

public final class Sort implements Expression {
    public enum Direction {
        ASCENDING, DESCENDING
    }

    private final Expression _sequence;
    private final Direction _direction;

    public Sort(final Expression sequence, final Direction direction) {
        _sequence = sequence;
        _direction = direction;
    }

    public Expression sequence() {
        return _sequence;
    }

    public Direction direction() {
        return _direction;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitSort(this);
    }

    public boolean constant() {
        return _sequence.constant();
    }
}

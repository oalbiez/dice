package org.poloper.dice.expression;

public final class Var implements Expression {
    private final String _name;

    public Var(final String name) {
        _name = name;
    }

    public String name() {
        return _name;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitVar(this);
    }

    public boolean constant() {
        return true;
    }
}

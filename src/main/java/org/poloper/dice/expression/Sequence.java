package org.poloper.dice.expression;

import io.vavr.collection.List;

public final class Sequence implements Expression {
    private final List<Expression> _items;

    Sequence(final List<Expression> items) {
        _items = items;
    }

    public List<Expression> items() {
        return _items;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitSequence(this);
    }

    public boolean constant() {
        return _items.forAll(Expression::constant);
    }
}

package org.poloper.dice.expression;

public interface ExpressionVisitor<T> {
    T visitBinaryOperator(BinaryOperator expression);

    T visitBooleanLiteral(BooleanLiteral expression);

    T visitComparison(Comparison expression);

    T visitDice(Dice expression);

    T visitDiceFate(DiceFate expression);

    T visitHasTags(HasTags expression);

    T visitLet(Let expression);

    T visitName(Name expression);

    T visitNumberLiteral(NumberLiteral expression);

    T visitRepeat(Repeat expression);

    T visitRollUntil(RollUntil expression);

    T visitSequence(Sequence expression);

    T visitSort(Sort expression);

    T visitTags(Tags expression);

    T visitTake(Take expression);

    T visitUnary(UnaryOperator expression);

    T visitVar(Var expression);
}

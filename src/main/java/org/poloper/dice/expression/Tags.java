package org.poloper.dice.expression;

import io.vavr.collection.List;

public final class Tags implements Expression {
    private final Expression _expression;
    private final List<Case> _cases;

    public Tags(final Expression expression, final List<Case> cases) {
        _expression = expression;
        _cases = cases;
    }

    public Expression expression() {
        return _expression;
    }

    public List<Case> cases() {
        return _cases;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitTags(this);
    }

    public boolean constant() {
        return _expression.constant();
    }
}

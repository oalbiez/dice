package org.poloper.dice.expression;

import io.vavr.collection.List;

public final class Expressions {

    public static Expression D(final int faces) {
        return dice(faces);
    }

    public static Expression DF() {
        return new DiceFate();
    }

    public static Expression abs(final Expression child) {
        return new UnaryOperator(UnaryOperator.Operator.ABS, child);
    }

    public static Expression and(final Expression... items) {
        return and(sequence(items));
    }

    public static Expression and(final Expression sequence) {
        return new BinaryOperator(BinaryOperator.Operator.AND, sequence);
    }

    public static Expression ascending(final Expression sequence) {
        return new Sort(sequence, Sort.Direction.ASCENDING);
    }

    public static Expression ascending(final Expression... items) {
        return new Sort(sequence(items), Sort.Direction.ASCENDING);
    }

    public static Expression constant(final int value) {
        return new NumberLiteral(value);
    }

    public static Expression constant(final boolean value) {
        return new BooleanLiteral(value);
    }

    public static Expression descending(final Expression sequence) {
        return new Sort(sequence, Sort.Direction.DESCENDING);
    }

    public static Expression descending(final Expression... items) {
        return new Sort(sequence(items), Sort.Direction.DESCENDING);
    }

    public static Expression dice(final int faces) {
        return new Dice(faces);
    }

    public static Expression eq(final Expression left, final Expression right) {
        return new Comparison(Comparison.Operator.EQ, left, right);
    }

    public static Expression eq(final int value) {
        return eq(constant(value));
    }

    public static Expression eq(final Expression right) {
        return new Comparison(Comparison.Operator.EQ, var("_"), right);
    }

    public static Expression explode(final Expression roll, final Expression predicate) {
        return sum(rollUntil(roll, predicate));
    }

    public static Expression first(final Expression sequence) {
        return first(constant(1), sequence);
    }

    public static Expression first(final int n, final Expression sequence) {
        return first(constant(n), sequence);
    }

    public static Expression first(final Expression n, final Expression sequence) {
        if (!n.constant()) {
            throw new IllegalArgumentException("n should be constant");
        }
        return new Take(Take.Operator.FIRST, n, sequence);
    }

    public static Expression ge(final Expression left, final Expression right) {
        return new Comparison(Comparison.Operator.GE, left, right);
    }

    public static Expression ge(final int value) {
        return ge(constant(value));
    }

    public static Expression ge(final Expression right) {
        return new Comparison(Comparison.Operator.GE, var("_"), right);
    }

    public static Expression gt(final Expression left, final Expression right) {
        return new Comparison(Comparison.Operator.GT, left, right);
    }

    public static Expression gt(final int value) {
        return gt(constant(value));
    }

    public static Expression gt(final Expression right) {
        return new Comparison(Comparison.Operator.GT, var("_"), right);
    }

    public static Expression keepHighest(final Expression roll, final Expression poolSize, final Expression keep) {
        return first(keep, descending(repeat(poolSize, roll)));
    }

    public static Expression keepLowest(final Expression roll, final Expression poolSize, final Expression keep) {
        return first(keep, ascending(repeat(poolSize, roll)));
    }

    public static Expression last(final Expression sequence) {
        return last(constant(1), sequence);
    }

    public static Expression last(final int n, final Expression sequence) {
        return last(constant(n), sequence);
    }

    public static Expression last(final Expression n, final Expression sequence) {
        if (!n.constant()) {
            throw new IllegalArgumentException("n should be constant");
        }
        return new Take(Take.Operator.LAST, n, sequence);
    }

    public static Expression le(final Expression left, final Expression right) {
        return new Comparison(Comparison.Operator.LE, left, right);
    }

    public static Expression le(final int value) {
        return le(constant(value));
    }

    public static Expression le(final Expression right) {
        return new Comparison(Comparison.Operator.LE, var("_"), right);
    }

    public static Expression lt(final Expression left, final Expression right) {
        return new Comparison(Comparison.Operator.LT, left, right);
    }

    public static Expression lt(final int value) {
        return lt(constant(value));
    }

    public static Expression lt(final Expression right) {
        return new Comparison(Comparison.Operator.LT, var("_"), right);
    }

    public static Expression max(final Expression... items) {
        return max(sequence(items));
    }

    public static Expression max(final Expression sequence) {
        return new BinaryOperator(BinaryOperator.Operator.MAX, sequence);
    }

    public static Expression min(final Expression... items) {
        return min(sequence(items));
    }

    public static Expression min(final Expression sequence) {
        return new BinaryOperator(BinaryOperator.Operator.MIN, sequence);
    }

    public static Expression mul(final Expression... items) {
        return mul(sequence(items));
    }

    public static Expression mul(final Expression sequence) {
        return new BinaryOperator(BinaryOperator.Operator.MUL, sequence);
    }

    public static Expression name(final Expression child, final String name) {
        return new Name(child, name);
    }

    public static Expression neg(final Expression child) {
        return new UnaryOperator(UnaryOperator.Operator.NEG, child);
    }

    public static Expression neq(final Expression left, final Expression right) {
        return new Comparison(Comparison.Operator.NEQ, left, right);
    }

    public static Expression neq(final int value) {
        return neq(constant(value));
    }

    public static Expression neq(final Expression right) {
        return new Comparison(Comparison.Operator.NEQ, var("_"), right);
    }

    public static Expression not(final Expression child) {
        return new UnaryOperator(UnaryOperator.Operator.NOT, child);
    }

    public static Expression or(final Expression... items) {
        return or(sequence(items));
    }

    public static Expression or(final Expression sequence) {
        return new BinaryOperator(BinaryOperator.Operator.OR, sequence);
    }

    public static Expression reRoll(final Expression roll, final Expression predicate, final Expression limit) {
        return sum(last(rollUntil(roll, predicate, limit)));
    }

    public static Expression repeat(final int n, final Expression roll) {
        return repeat(constant(n), roll);
    }

    public static Expression repeat(final Expression n, final Expression roll) {
        if (!n.constant()) {
            throw new IllegalArgumentException("n should be constant");
        }
        return new Repeat(n, roll);
    }

    public static Expression rollUntil(final Expression roll, final Expression predicate) {
        return rollUntil(roll, predicate, constant(true));
    }

    public static Expression rollUntil(final Expression roll, final Expression predicate, final Expression limit) {
        if (roll.constant()) {
            throw new IllegalArgumentException("roll should not be constant");
        }
        return new RollUntil(roll, predicate, limit);
    }

    public static Expression sequence(final Expression... items) {
        return sequence(List.of(items));
    }

    public static Expression sequence(final List<Expression> items) {
        return new Sequence(items);
    }

    public static Expression sub(final Expression... items) {
        return sub(sequence(items));
    }

    public static Expression sub(final Expression sequence) {
        return new BinaryOperator(BinaryOperator.Operator.SUB, sequence);
    }

    public static Expression sum(final Expression... items) {
        return sum(sequence(items));
    }

    public static Expression sum(final Expression sequence) {
        return new BinaryOperator(BinaryOperator.Operator.ADD, sequence);
    }

    public static Expression tags(final Expression expression, final Case... cases) {
        return tags(expression, List.of(cases));
    }

    public static Expression tags(final Expression expression, final List<Case> cases) {
        return new Tags(expression, cases);
    }

    public static Expression var(final String name) {
        return new Var(name);
    }

    public static Case when(final Expression condition, final String label) {
        return new Case(condition, label);
    }

    public static Definition def(final String name, final Expression value) {
        return new Definition(name, value);
    }

    public static Expression with(final Expression expression, final Definition... definitions) {
        return with(expression, List.of(definitions));
    }

    public static Expression with(final Expression expression, final List<Definition> definitions) {
        return new Let(definitions, expression);
    }

    public static Expression hasTag(final Expression expression, final String name) {
        return new HasTags(expression, List.of(name));
    }
}

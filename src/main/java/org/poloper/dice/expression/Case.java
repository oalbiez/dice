package org.poloper.dice.expression;

public final class Case {
    private final Expression _expression;
    private final String tag;

    public Case(final Expression expression, final String tag) {
        _expression = expression;
        this.tag = tag;
    }

    public Expression expression() {
        return _expression;
    }

    public String tag() {
        return tag;
    }
}

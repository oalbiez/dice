package org.poloper.dice.expression;

public final class Name implements Expression {
    private final Expression _child;
    private final String _name;

    public Name(final Expression child, final String name) {
        _child = child;
        _name = name;
    }

    public Expression child() {
        return _child;
    }

    public String name() {
        return _name;
    }

    public <T> T accept(final ExpressionVisitor<T> visitor) {
        return visitor.visitName(this);
    }

    public boolean constant() {
        return _child.constant();
    }
}
